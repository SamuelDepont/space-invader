#!/bin/bash

# Compilation du fichier JavaFX
javac -d bin/ --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls,javafx.media src/*.java

# Exécution du fichier compilé
java  -cp bin --module-path /usr/share/openjfx/lib/  --add-modules javafx.controls,javafx.media SpaceInvader