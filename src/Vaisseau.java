public class Vaisseau {
    /** position horizontale du vaisseau*/
    private double posx;
    /** dessin du vaisseau*/
    private EnsembleChaines Vaisseauchaines;

    /**
     * Construteur du vaisseau (crée le vaisseau)
     * @param x double
     */
    public Vaisseau(double x){
        this.posx=x;
        this.Vaisseauchaines = new EnsembleChaines();
        this.Vaisseauchaines.ajouteChaine(posx,5.0,"      ▄      ");
        this.Vaisseauchaines.ajouteChaine(posx,4.0,"     ███     ");
        this.Vaisseauchaines.ajouteChaine(posx,3.0,"▄███████████▄");
        this.Vaisseauchaines.ajouteChaine(posx,2.0,"█████████████");
        this.Vaisseauchaines.ajouteChaine(posx,1.0,"█████████████");
    }
    /**
     * retourne la position horizontale du vaisseau
     * @return
     */
    public double getPosx(){
        return this.posx;
    }
    /**
     * deplace de vaisseau d'un certain nombre
     * @param dx double
     */
    public void deplace(double dx){
        this.posx += dx;
        for(ChainePositionnee chaineVaisseau : this.Vaisseauchaines.chaines){
            chaineVaisseau.x += dx;
        }
    }
    /**Retourne la liste de chaînes vaisseauChaine
     * @return EnsembleChaines
    */
    public EnsembleChaines getEnsembleChaines(){
        return this.Vaisseauchaines;
    }
    /**
     * retourne la position du canon
     * @return
     */
    public double getpositionCanon(){
        double canonPosX = this.posx + 6;
        return canonPosX;
    }
    
}
