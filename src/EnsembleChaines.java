import java.util.ArrayList;
public class EnsembleChaines {
    ArrayList<ChainePositionnee> chaines;
    public EnsembleChaines(){chaines= new ArrayList<ChainePositionnee>(); }

    public void ajouteChaine(Double x, Double y, String c){
        chaines.add(new ChainePositionnee(x,y,c));}

    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }
    /**

    * Vérifie si une position spécifiée est contenue dans l'une des chaînes positionnées 
    * @param x la coordonnée x de la position à vérifier
    * @param y la coordonnée y de la position à vérifier
    * @return true si la position se trouve dans une chaîne positionnée , false sinon
    */
    public boolean contient(int x, int y) {
        for (ChainePositionnee c : chaines) {
            if (c.contient(x, y)) {
                return true;
            }
        }
        return false;
    }
    
    
}
