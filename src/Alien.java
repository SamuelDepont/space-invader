import javafx.scene.media.*;
import javafx.scene.media.MediaPlayer;
import java.io.File;

public class Alien {
    /** position horizontale de l'alien*/
    private double positionx;
    /** position verticale de l'alien */
    private double positiony;
    /** dessin de l'alien 1 */
    private EnsembleChaines AlienChaine;
    /** dessin de l'alien 2 */
    private EnsembleChaines AlienChaine2;
    /** nombre de tours */
    private int tours;
    /**
     * Construteur de l'alien (crée un alien)
     * @param x position horizontale de l'alien
     * @param y position verticale de l'alien
     */
    public Alien(double x,Double y){
        this.positionx=x;
        this.positiony=y;
        this.tours = 0;
        this.AlienChaine = new EnsembleChaines();
        this.AlienChaine.ajouteChaine(positionx,positiony+6,"             ");
        this.AlienChaine.ajouteChaine(positionx,positiony+5,"   ▀▄   ▄▀   ");
        this.AlienChaine.ajouteChaine(positionx,positiony+4,"  ▄█▀███▀█▄  ");
        this.AlienChaine.ajouteChaine(positionx,positiony+3," █▀███████▀█ ");
        this.AlienChaine.ajouteChaine(positionx,positiony+2," █ █▀▀▀▀▀█ █ ");
        this.AlienChaine.ajouteChaine(positionx,positiony+1,"    ▀▀ ▀▀    ");
        this.AlienChaine.ajouteChaine(positionx,positiony  ,"             ");
        this.AlienChaine2 = new EnsembleChaines();
        this.AlienChaine2.ajouteChaine(positionx,positiony+6,"             ");
        this.AlienChaine2.ajouteChaine(positionx,positiony+5,"   ▀▄   ▄▀   ");
        this.AlienChaine2.ajouteChaine(positionx,positiony+4," █ █▀███▀█ █ ");
        this.AlienChaine2.ajouteChaine(positionx,positiony+3," ▀█████████▀ ");
        this.AlienChaine2.ajouteChaine(positionx,positiony+2,"   █▀▀▀▀▀█   ");
        this.AlienChaine2.ajouteChaine(positionx,positiony+1,"    ▀   ▀    ");
        this.AlienChaine2.ajouteChaine(positionx,positiony  ,"             ");

    }


    /**
     * Retourne la position horizontale actuelle de l'alien
     * @return double 
     */
    public double getPositionX() {
        return this.positionx;
    }
    /**
     * Retourne la position verticale actuelle de l'alien
     * @return double 
     */
    public double getPositionY() {
        return this.positiony;
    }
    /**Retourne la liste de chaînes AlienChaine
     * @return EnsembleChaines
    */
    public EnsembleChaines getEnsembleChaines(){
        return this.AlienChaine;
    }
    /**Retourne la liste de chaînes AlienChainev2
     * @return EnsembleChaines
    */
    public EnsembleChaines getEnsembleChainesv2(){
        return this.AlienChaine2;
    }

    /** Deplace l'alien en position x et y */
    public void evolue() {
        if  (this.tours < 100) {
            this.positionx += 0.1;
            for (ChainePositionnee chainePositionnee : this.AlienChaine.chaines) {
                chainePositionnee.x += 0.1;
            }
            for (ChainePositionnee chainePositionnee : this.AlienChaine2.chaines) {
                chainePositionnee.x += 0.1;
            }
        }
        if (this.tours == 100 || this.tours == 1) {
            this.positiony -= 0.7;
            for (ChainePositionnee chainePositionnee : this.AlienChaine.chaines) {
                chainePositionnee.y -= 0.7;
            }
            for (ChainePositionnee chainePositionnee : this.AlienChaine2.chaines) {
                chainePositionnee.y -= 0.7;
            }
        }
        if  (this.tours > 100) {
            this.positionx -= 0.1;
            for (ChainePositionnee chainePositionnee : this.AlienChaine.chaines) {
                chainePositionnee.x -= 0.1;
            }
            for (ChainePositionnee chainePositionnee : this.AlienChaine2.chaines) {
                chainePositionnee.x -= 0.1;
            }
        }
        if(this.tours == 200){
            this.tours = 0;
        }
        this.tours += 1;
    }
    /** Quand un alien est tuer un bruit est jouer  */
    public void sonAlienTuer(){
        final File file = new File("son/son_alien.mp3"); //lien du son
        final Media media = new Media(file.toURI().toString()); 
        final MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.play();
    }
    /**
     * Vérifie si les coordonnées (a,b) sont sur l'image de l'alien. 
     * @param a double
     * @param b bouble
     * @return true ou false
     */
    public boolean contient(double a, double b) {
        double alienX = this.getPositionX();
        double alienY = this.getPositionY();
        double largeur = 13; // Largeur de l'alien
        double hauteur = 7; // Hauteur de l'alien

        // Vérifie si les coordonnées (x,y) sont sur l'image de l'alien
        if (a >= alienX  && a <= alienX + largeur && b >= alienY && b <= alienY + hauteur) {
            return true;
        }
        return false;
    }
}