public class ChainePositionnee{
    double x,y;
    String c;
    public ChainePositionnee(Double a,Double b, String d){x=a; y=b; c=d;}
    /**
    * Vérifie si la position spécifiée se trouve dans la chaîne de caractères positionnée.
    * @param x la coordonnée x de la position à vérifier
    * @param y la coordonnée y de la position à vérifier
    * @return true si la position se trouve dans la chaîne de caractères positionnée, false sinon
    */
    public boolean contient(int x, int y) {
        return this.x == x && this.y == y;
    }
}
