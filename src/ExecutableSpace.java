import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.application.Platform;
import java.util.Optional;




public class ExecutableSpace extends Application {
    private Pane root;
    private Group caracteres;
    private GestionJeu gestionnaire;
    private int hauteurTexte;
    private int largeurCaractere;
    private Alert messageFin;
    private boolean messageAffichee = false;


    public static void main(String[] args) {
        launch(args);
    }

    private void afficherCaracteres(){
        caracteres.getChildren().clear();
        int hauteur = (int) root.getHeight();
        for( ChainePositionnee c : gestionnaire.getChaines().chaines)
        {
            Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            caracteres.getChildren().add(t);
        }
    }

    private void lancerAnimation() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            gestionnaire.jouerUnTour();
                            afficherCaracteres();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
    
    /**
    * Met à jour l'affichage du message de fin de partie si la partie est gagnée ou perdue, et que le message n'a pas déjà été affiché.
    */
    private void maj() {
        if (!messageAffichee && this.gestionnaire.estGagnee() || !messageAffichee && this.gestionnaire.estPerdu()) {
            this.messageFinDePartie();
            messageAffichee = true; // message affiché, on défini la variable sur true
        }
    }
    /**
    * Affiche un message de fin de partie avec le score et propose de rejouer ou de quitter le jeu.
    * Si l'utilisateur clique sur "Oui", le jeu est relancé.
    * Si l'utilisateur clique sur "Non", le jeu est désactivé.
    */
    public void messageFinDePartie(){
        String message = "";
        if (this.gestionnaire.estGagnee()){
            message = "Vous avez gagné ! " +gestionnaire.getScore();
        }else{
            message = "Vous avez perdu ! ";
        }
        messageFin = new Alert(Alert.AlertType.CONFIRMATION, message+"\nVoulez-vous rejouer ?",ButtonType.YES, ButtonType.NO);
        messageFin.setTitle("Attention");
        Optional<ButtonType> rep = messageFin.showAndWait();
        if (rep.isPresent() && rep.get()==ButtonType.YES){
            this.rejouer();
        }
        else{
            this.desactiver();
        }
    }
    

    /** relance le jeu  */
    public void rejouer(){
        messageFin.close(); // ferme le message de fin
        Stage currentStage = (Stage) root.getScene().getWindow();
        currentStage.close(); // ferme la fenêtre courante
        SpaceInvader nouvellepartie = new SpaceInvader();
        Stage newStage = new Stage();
        nouvellepartie.start(newStage);
    }
    
    /** quitte le jeu  */
    public void desactiver() {
        Platform.runLater(() -> {
            Stage stage = (Stage) root.getScene().getWindow();
            stage.close();
            Platform.exit();
        });
    }
    

    @Override
        public void start(Stage primaryStage) {
            primaryStage.setTitle("IUTO Space Invader");
            Image imageFond = new Image("file:img/espace.jpg");
            BackgroundSize backgroundSize = new BackgroundSize(700, 700, false, false, false, false);
            BackgroundImage backgroundImage = new BackgroundImage(imageFond, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, backgroundSize);
            Background background = new Background(backgroundImage);
            caracteres = new Group();
            root= new AnchorPane(caracteres);
            gestionnaire = new GestionJeu();
            Text t=new Text("█");
            t.setFont(Font.font("Monospaced",10));
            hauteurTexte =(int) t.getLayoutBounds().getHeight();
            largeurCaractere = (int) t.getLayoutBounds().getWidth();

            Scene scene = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                if(key.getCode()==KeyCode.LEFT)
                    gestionnaire.toucheGauche();
                    this.maj();
                if(key.getCode()==KeyCode.RIGHT)
                    gestionnaire.toucheDroite();
                    this.maj();
                if(key.getCode()==KeyCode.SPACE)
                    gestionnaire.toucheEspace();
                    this.maj();
            });
            root.setBackground(background);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
            lancerAnimation();

        }
}

