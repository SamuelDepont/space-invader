import javafx.scene.media.*;
import javafx.scene.media.MediaPlayer;
import java.io.File;
public class Projectile {
    /** position horizontale du projectile */
    private double positionx;
    /** position vertical du projectile */
    private double positiony;
    /** dessin du projectile */
    private EnsembleChaines Projectilechaines;
    /**
     * Construteur du projectile
     * @param posx position horizontale du projectile
     * @param posy position vertical du projectile
     */
    public Projectile(Double posx, Double posy){
        this.positionx = posx; 
        this.positiony = posy;
        this.Projectilechaines = new EnsembleChaines();
        this.Projectilechaines.ajouteChaine(positionx,positiony  , "⮝");
    }
    /**
     * retourne position vertical du projectile
     * @return double
     */
    public double getPositionY(){
        return this.positiony;
    }
    /**
     * retourne la position vertical du projectile
     * @return double
     */
    public double getPositionX(){
        return this.positionx;
    }
    /**Retourne la liste de chaînes ProjectileChaine
     * @return EnsembleChaines
    */
    public EnsembleChaines getEnsembleChaines(){
        return this.Projectilechaines;
    }

    /** deplace le projectile en y */
    public void evolue() {
        this.positiony += 0.2;
        for(ChainePositionnee chaineProjecttile : this.Projectilechaines.chaines){
            chaineProjecttile.y += 0.2;
        }

    }
    /** Quand un projectile est tirer un bruit est jouer  */
    public void SonProjectileToucher(){
        final File file = new File("son/son_tire.mp3"); 
        final Media media = new Media(file.toURI().toString()); 
        final MediaPlayer mediaPlayer = new MediaPlayer(media); 
        mediaPlayer.play();
    }
    
    @Override
    public String toString(){
        return "Projectile a la position : ("+this.positionx+","+this.positiony+")";
    }
}
