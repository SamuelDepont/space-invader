import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.geometry.Insets;



/** menu du SpaceInvader */
public class SpaceInvader extends Application {
    private Stage primaryStage;


    public Stage getPrimaryStage() {
        return primaryStage;
    }

    private VBox vbox(){
        VBox pane =new VBox(25);
        ImageView image = new ImageView("file:img/menu.jpg");
        Button boutonJ =new Button("Jouer");
        pane.setAlignment(Pos.CENTER);  
        image.setPreserveRatio(true);
        image.setFitHeight(300);
        
        boutonJ.setOnAction(new ControleurJouer(this));
        boutonJ.setPrefSize(100, 50);
        pane.setPadding(new Insets(0, 0, 200, 0));
        pane.getChildren().addAll(image, boutonJ);

        return pane;
    }

    private VBox root() {
        VBox pane = new VBox();
        pane.setStyle("-fx-background-color: black;");
        pane.setAlignment(Pos.CENTER);
        VBox vbox = new VBox();
        vbox.getChildren().addAll(vbox());
        vbox.setPrefWidth(400);
        vbox.setAlignment(Pos.CENTER);
        pane.getChildren().add(vbox);
        return pane;
    }


    @Override
    public void start(Stage stage){    
        this.primaryStage = stage;
        Scene scene =new Scene(root(), 600, 700);
        primaryStage.setTitle("SpaceInvader");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

