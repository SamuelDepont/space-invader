import java.util.ArrayList;
import java.util.List;


public class GestionJeu {
    /** largeur du jeu */
    private int Largeur;
    /** Hauteur du jeu */
    private int Hauteur;
    /** ensemble des chaine */
    private EnsembleChaines chaines;
    /** le vaiseau */
    private Vaisseau vaisseau;
    /** le projectile */
    private Projectile projectile;
    /** liste des projectiles toucher  */
    private List<Projectile> projectileToucher;
    /** liste des aliens */
    private List<Alien> aliens;
    /** liste des aliens toucher  */
    private List<Alien> alienToucher;
    /** score du jeu */
    private Score score;
    /**  temps du style des aliens  */
    private long dernierChangementStyle; // variable pour stocker le temps écoulé depuis le dernier changement de style
    /** style des aliens */
    private int alienStyle;

    /** Construteur de GestionJeu */
    public GestionJeu(){
        this.Largeur=100;
        this.Hauteur=60;
        this.chaines= new EnsembleChaines();
        this.vaisseau = new Vaisseau(0);
        this.chaines.union(this.vaisseau.getEnsembleChaines());
        this.projectile = null;
        this.score = new Score();
        creerlesAliens();
        this.dernierChangementStyle = System.currentTimeMillis();
        this.alienStyle = 1;
    }
    
    /**
     * retourne la Hauteur
     * @return int
     */
    public int getHauteur(){
        return this.Hauteur;
    }
    /**
     * retourne la largeur
     * @return int
     */
    public int getLargeur(){
        return this.Largeur;
    }
    /**
     * retourne true ou false si les aliens son en dehors de l'ecran 
     * @return boolean
     */
    public boolean estPerdu() {
        for (Alien alien : aliens) {
            if (alien.getPositionY()  <= -1) {
                return true;
            }
        }
        return false;
    }
        
    /**
     * retourne true ou false si il y a plus d'aliens
     * @return
     */
    public boolean estGagnee(){
        if(this.aliens.size() == 0){
            return true;
        }
        return false;
    }
    /** Crée les aliens sur le jeu */
    public void creerlesAliens(){
        this.aliens = new ArrayList<>();
        double posx = 2;
        double posy = 50;
        for (int i=0; i<9; i++){
            this.aliens.add(new Alien(posx, posy));
            posx +=18;
            if ( posx > 85){
                posx=2;
                posy -=10;
            }
        }
    }
    /**
     * retourne le Score
     * @return Score
     */
    public Score getScore() {
        return score;
    }
    /** deplace le vaisseau de 1 a gauche  */
    public void toucheGauche(){
        if (this.vaisseau.getPosx() > 0) { // Vérifie si le vaisseau dépasse le bord gauche
            System.out.println("Appuyer touche gauche");
            this.vaisseau.deplace(-1);
            
        }
    }
    /** deplace le vaisseau de 1 a droite  */
    public  void toucheDroite(){
        if (this.vaisseau.getPosx() < this.Largeur - 13 ) { // Vérifie si le vaisseau dépasse le bord droit
            System.out.println("Appuyer touche droite");
            this.vaisseau.deplace(1);

        }
    }
    /** Crée un projectile si on appuie sur la touche espace et si il y a pas de projectile sur le jeu */
    public void toucheEspace(){
        System.out.println("Appuyer touche espace");
        if(this.projectile == null){
            Projectile nouveauProjectile = new Projectile(vaisseau.getpositionCanon(), 5.0);
            this.projectile = nouveauProjectile; 
            this.chaines.union(nouveauProjectile.getEnsembleChaines());
            this.projectile.SonProjectileToucher();
        }
    }
    /**
     * retourne l'ensemble des chaines
     * @return EnsembleChaine
     */
    public EnsembleChaines getChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        if(this.vaisseau != null) chaines.union(this.vaisseau.getEnsembleChaines());
        if(this.projectile != null) chaines.union(this.projectile.getEnsembleChaines());
        for(Alien alien : this.aliens){
            chaines.union(animerAliens());

        }
        return chaines;
    }
    /**
     * anime les aliens ( change l'ensemble des chaine  toute les 0.8 s)
     * @return EnsembleChaine 
     */
    public EnsembleChaines animerAliens() {
        EnsembleChaines nouveauAlien = new EnsembleChaines();
        long tempsActuel = System.currentTimeMillis(); // stocke le temps actuel
        if (tempsActuel - this.dernierChangementStyle >= 800) { // vérifie si 0.8 secondes se sont écoulées depuis le dernier changement de style
            this.alienStyle = (this.alienStyle == 1) ? 2 : 1; // Si la valeur actuelle est égale à 1, alors la nouvelle valeur sera 2, sinon la nouvelle valeur sera 1
            this.dernierChangementStyle = tempsActuel; // met à jour la variable du dernier changement de style
        }
        for (Alien alien : this.aliens) {
            if (this.alienStyle == 1) {
                nouveauAlien.union(alien.getEnsembleChaines());
            } else {
                nouveauAlien.union(alien.getEnsembleChainesv2());
            }
        }
        return nouveauAlien;
    }

    /**
    * Vérifie si le projectile touche un alien et le supprime de la liste des aliens s'il est touché.
    * Ajoute l'alien touché dans la liste "alienToucher" et le projectile touché dans la liste "projectileToucher".
    * Si tous les aliens ont été tués, appelle la méthode "estGagnee()" pour vérifier si le joueur a gagné.
    */    
    public void alienToucher(){
        this.projectileToucher = new ArrayList<>();
        this.alienToucher = new ArrayList<>();
        for (int i = 0; i < aliens.size(); i++) {
            Alien alien = aliens.get(i);
            if(this.projectile != null){
                if (alien.contient(this.projectile.getPositionX(), this.projectile.getPositionY())) {
                    // Suppression de l'alien touché
                    alien.sonAlienTuer();
                    this.alienToucher.add(new Alien(this.projectile.getPositionX(), this.projectile.getPositionY()));
                    aliens.remove(i);
                    this.projectileToucher.add(new Projectile(this.projectile.getPositionX(), this.projectile.getPositionY()));
                    this.projectile = null;
                    System.out.println("TOUCHE");
                    this.estGagnee();
                }
            }
        }
    }

    /**
    * Effectue un tour de jeu, faisant évoluer les éléments et vérifiant les conditions de fin de partie.
    */
    public void jouerUnTour(){
        // Vérifier si le projectile est en mouvement
        if (this.projectile != null) {
            projectile.evolue();
            // Vérifier si le projectile a atteint le bord supérieur de l'écran
            if (this.projectile.getPositionY() >= this.getHauteur()) {
                this.projectile = null;
            } else {
                // Vérifier si le projectile touche un alien
                this.alienToucher();
            }
        }
        // Vérifier si le joueur a gagné ou perdu
        this.estGagnee();
        this.estPerdu();
        // Faire évoluer les aliens
        for (int i = 0; i < aliens.size(); i++) {
            Alien alien = aliens.get(i);
            alien.evolue();
        }
        // Ajouter un point au score
        score.ajouter(1);
    }
}




