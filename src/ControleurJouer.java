import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class ControleurJouer implements EventHandler<ActionEvent>{
    
    /** L'application SpaceInvader. */
    private SpaceInvader appli;
    /**
    * Crée une nouvelle instance de la classe ControleurJouer 
    * @param appli l'application SpaceInvader
    */
    public ControleurJouer(SpaceInvader appli){
        this.appli = appli;
    }
    
    /**
    * Ferme la fenêtre principale et démarre le jeu SpaceInvader lorsque le bouton jouer est déclenché.
    * @param e l'événement ActionEvent
    */
    public void handle(ActionEvent e){
        Stage stage = new Stage();
        appli.getPrimaryStage().close(); // fermer la fenêtre principale
        ExecutableSpace es = new ExecutableSpace();
        es.start(stage);
    }
}